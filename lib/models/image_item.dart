import 'package:flutter/foundation.dart';

class ImageItemModel extends ChangeNotifier {
  final String userName;
  final String description;
  final String thumbSrc;
  final String regularSrc;

  ImageItemModel({
    this.userName,
    this.description,
    this.thumbSrc,
    this.regularSrc,
  });

  factory ImageItemModel.fromJson(Map<String, dynamic> json) {
    return ImageItemModel(
      userName: json['user']['name'] ?? 'Unknown author',
      description: _capitalize(json['alt_description'] ?? 'no description'),
      thumbSrc: json['urls']['small'],
      regularSrc: json['urls']['regular'],
    );
  }

  static String _capitalize(String s) => s[0].toUpperCase() + s.substring(1);
}
